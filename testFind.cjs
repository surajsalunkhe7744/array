
const find = require("./find.cjs")
const items = [1,2,3,4,5,5];

function callBackFunction(arr,newarr)
{
    for(index=0;index<arr.length;index++)
    {
        if(arr[index]%2==1)
        newarr.push(arr[index]);
        else
        newarr.push(undefined);
    }
    return;
}

console.log(find(items, callBackFunction));

console.log(find(items, "ds"));

console.log(find(items, {}));

console.log(find([], callBackFunction));