const testFlatten = require("./flatten.cjs");

let items = [1,[2],[[3]],[[[4]]]];

console.log(testFlatten(items));

console.log(testFlatten({}));

console.log(testFlatten(123));

console.log(testFlatten("string"));