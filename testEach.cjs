const testEach = require("./each.cjs");

const items = [1, 2, 3, 4, 5, 5];

function callBackFunction(element)
{
    if(typeof element != 'undefined')
    return element*2;
}

console.log(testEach(items,callBackFunction));

console.log(testEach(items,[]));

console.log(testEach([],callBackFunction));

console.log(testEach());