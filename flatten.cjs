
//let items = [1,[2],[[3]],[[[4]]]];


function flatten(elements,depth=1){
    // if(depth == undefined)
    // {
    //     depth = 1;
    // }
    // if(depth <= 0)
    // {
    //     return elements;
    // }
    
    if(Array.isArray(elements) && typeof(depth) === 'number'){
        let newArr=[];
        for(let index=0;index<elements.length;index++){
            if(elements[index] === undefined){
                continue;
            }
            //console.log(456);
            newArr = newArr.concat(elements[index]);
        }
        depth--;
        if(depth>0){
            return flatten(newArr,depth);
        }
        else{
            return newArr;
        }
    }
    return 0;
}

//let arr = [1, 2, [[[3,4]]]];

//console.log(arr.flat({}));
//console.log(flatten(arr,{}));

module.exports = flatten;