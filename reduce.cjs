
function reduce(items,callBack,startingValue){
    if(typeof(items)=='object' && Array.isArray(items)){
        let index=0;
        if(typeof(startingValue)=='undefined'){
            startingValue=items[0];
            index=1;
        }
        let out=0;
        for (let Index=index; Index<items.length; Index++){
            if(startingValue != undefined)
            out=callBack(startingValue,items[Index], Index, items)
            startingValue=out;
        }
        return out;
        }

        if(typeof(items)!='object' && Array.isArray(items) != true)
        return 0;
        
    
    
}

module.exports = reduce;



