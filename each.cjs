//const items = [1, 2, 3, 4, 5, 5];

/*
function callBackFunction(element)
{
    if(typeof element != 'undefined')
    return element*2;
}
*/

function each(items,callBack)
{
let newArr = [];
if((Array.isArray(items)) && typeof callBack == 'function')
{
    for(index=0;index<items.length;index++)
    {
       newArr.push(callBack(items[index]));
    }
    return newArr;
}
else
return [];

}

//console.log(each(items,callBackFunction));

module.exports = each;

